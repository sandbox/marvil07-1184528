<?php
/**
 * @file
 * Plain text version of file fields.
 */

/**
 * File generators.
 */
class FilePlainGenerator implements PlainGenerator {

  public function getPlainField($field_item) {
    if (empty($field_item['fid'])) {
      return FALSE;
    }
    $file = file_load($field_item['fid']);
    $file_object = new stdClass;
    $file_object->filepath = drupal_realpath($file->uri);
    $file_object->filename = $file->filename;
    if (($plain=$this->getPlainText($file_object)) !== FALSE) {
      return $plain;
    }
  }

  /**
   * Get the plain text contents of the file.
   *
   * @param $file
   *   A standard Drupal file object, as expected by mimedetect_mime().
   *   a.k.a. it needs to have filepath and filename properties.
   *
   * @return
   *   The string containing the content of the file in plain text, or
   *   FALSE in case the contents can not be retrieved.
   */
  public function getPlainText($file) {
    $mime_type = mimedetect_mime($file);
    if (!is_string($mime_type) || empty($mime_type)) {
      return FALSE;
    }

    $filepath = drupal_realpath($file->filepath);

    $ret_val = null;
    switch ($mime_type) {
    case 'text/plain':
      $handle = fopen($filepath, 'r');
      $output = array(fread($handle, filesize($filepath)));
      fclose($handle);
      break;
    case 'application/pdf':
      exec("pdftotext $filepath -", $output, $ret_val);
      break;
    case 'application/postscript':
      $temp_file = tempnam(sys_get_temp_dir(), 'plain');
      exec("ps2pdf $filepath $temp_file", $output, $ret_val);
      if ($ret_val !== 0) { // shell fail, probably 127 (command not found)
        return FALSE;
      }
      exec("pdftotext $temp_file -", $output, $ret_val);
      unlink($temp_file);
      break;
    case 'application/msword':
      exec("catdoc $filepath", $output, $ret_val);
      break;
    case 'text/html':
      exec("html2text $filepath", $output, $ret_val);
      break;
    case 'text/rtf':
      // Note: correct handling of code pages in unrtf
      // on debian lenny unrtf v0.19.2 can not, but unrtf v0.20.5 can
      exec("unrtf --text $filepath", $output, $ret_val);
      if ($ret_val == 127) { // command not found
        return false;
      }
      // Avoid index unrtf comments
      if (is_array($output) && count($output) > 1) {
        $parsed_output = array();
        foreach ($output as & $line) {
          if (!preg_match('/^###/', $line, $matches)) {
            if (!empty($line)) {
              $parsed_output[] = $line;
            }
          }
        }
        $output = $parsed_output;
      }
      break;
    case 'application/vnd.ms-powerpoint':
      exec("catppt $filepath", $output, $ret_val);
      break;
    case 'application/vnd.ms-excel':
      exec("xls2csv -c\" \" $filepath", $output, $ret_val);
      break;
    }

    $content = '';
    if (!is_null($ret_val)) {
      if ($ret_val !== 0) { // shell fail, probably 127 (command not found)
        return FALSE;
      }
    }
    if (isset($output)) {
      foreach ($output as & $line) {
        $content .= $line."\n";
      }
      return $content;
    }
    else {
      return FALSE;
    }
  }

}
