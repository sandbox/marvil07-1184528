<?php
/**
 * @file
 * This plugin allow plain text version of file fields.
 */

$plugin = array(
  'title' => t('Plain file'),
  'handler' => array(
    'class' => 'FilePlainGenerator',
    'file' => 'FilePlainGenerator.inc',
    'path' => drupal_get_path('module', 'plain') . '/plugins/generators',
  ),
);
